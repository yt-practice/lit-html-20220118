import { App } from './app'
import { render } from 'preact'
import { html } from 'htm/preact'

render(html`<${App} />`, document.body)
