import { html } from 'htm/preact'
import type { ComponentChildren } from 'preact'
import { useState } from 'preact/hooks'

export const App = () => {
	return html`<div>
		<${Hoge} />
		<hr />
		<${Fuga} />
		<hr />
		<${Piyo} title="piyo">piyopiyo<//>
	</div>`
}

const Hoge = () => html`<div>hoge</div>`
const Fuga = () => {
	const [cnt, set] = useState(0)
	return html`<div>
		<button
			onClick="${(e: Event) => {
				e.preventDefault()
				set(v => v + 1)
			}}"
		>
			add
		</button>
		count: ${cnt}
	</div>`
}
const Piyo = (props: { title: string; children: ComponentChildren }) => {
	return html`<div>
		<h4>${props.title}</h4>
		<div>${props.children}</div>
	</div>`
}
